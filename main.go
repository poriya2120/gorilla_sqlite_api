package main

import (
	"fmt"
	"log"
	"net/http"
	
	"github.com/gorilla/mux"
)

func helloword(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hi Developer of Go")
}
func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/users", AllUsers).Methods("GET")
	myRouter.HandleFunc("/user/{name}/{email}", NewUser).Methods("POST")
	myRouter.HandleFunc("/user/{name}", DeleteUser).Methods("DELETE")
	myRouter.HandleFunc("/user/{name}/{email}", UpdateUser).Methods("PUT")

	myRouter.HandleFunc("/", helloword).Methods("GET")

	log.Fatal(http.ListenAndServe(":8181", myRouter))
}
func main() {
	fmt.Println("server is runnig")
	InitialMigration()
	handleRequests()
}
